# ics-ans-role-netbox

Ansible role to install netbox.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-netbox
```

## License

BSD 2-clause
